resource "google_compute_network" "vpc" {
  name                    = "${var.gcp_project_id}-gkedeploy-vpc"
  project                 = var.gcp_project_id
  auto_create_subnetworks = "false"
}

resource "google_compute_subnetwork" "subnet" {
  name                     = "${var.gcp_project_id}-gkedeploy-subnet"
  project                  = var.gcp_project_id
  region                   = var.gcp_default_region
  network                  = google_compute_network.vpc.name
  ip_cidr_range            = "10.10.0.0/24"
  private_ip_google_access = true

  depends_on = [
    google_compute_network.vpc
  ]
}

resource "google_compute_subnetwork" "lb-proxy-only-subnet" {
  name          = "${var.gcp_project_id}-gkedeploy-lb-proxy-only-subnet"
  project       = var.gcp_project_id
  region        = var.gcp_default_region
  network       = google_compute_network.vpc.name
  ip_cidr_range = "10.14.0.0/23"

  purpose = "REGIONAL_MANAGED_PROXY"
  role    = "ACTIVE"

  depends_on = [
    google_compute_network.vpc
  ]
}

resource "google_compute_router" "nat_router" {
  name    = "nat-router"
  project = var.gcp_project_id
  region  = var.gcp_default_region
  network = google_compute_network.vpc.name

  bgp {
    asn = 64514
  }
}

resource "google_compute_router_nat" "nat" {
  name                               = "nat-config"
  project                            = var.gcp_project_id
  router                             = google_compute_router.nat_router.name
  region                             = var.gcp_default_region
  nat_ip_allocate_option             = "AUTO_ONLY"
  source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_IP_RANGES"

  log_config {
    enable = true
    filter = "ERRORS_ONLY"
  }
}
