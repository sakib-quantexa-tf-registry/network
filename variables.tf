variable "gcp_project_id" {
  type        = string
  description = "The GCP project id"
}

variable "gcp_default_region" {
  type        = string
  description = "The GCP region where create the resources"
  default     = "europe-west2"
}

variable "gcp_default_zone" {
  type        = string
  description = "The GCP zone where create the resources"
  default     = "europe-west2-a"
}

variable "nat_router_name" {
  type        = string
  description = "The name of the NAT router"
}
